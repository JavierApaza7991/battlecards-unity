﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carta1Script : MonoBehaviour
{
    // Vriable para gestionar la velocidad desde Unity
    public float speed;

    // Variable para establecer un punto de destino
    Vector3 target;

    // Start is called before the first frame update
    void Start()
    {
        target = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void generateCard() {
        transform.position = Vector3.MoveTowards(transform.position, target, speed*Time.deltaTime);
    }
}
